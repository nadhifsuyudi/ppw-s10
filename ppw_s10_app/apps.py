from django.apps import AppConfig


class PpwS10AppConfig(AppConfig):
    name = 'ppw_s10_app'
