from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve
from ppw_s10_app import views
from selenium import webdriver
import time
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class HomepageTestCase(TestCase):
    def test_home_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_home_use_home_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_login_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code,200)

    def test_login_use_login_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'registration/login.html')

    def test_signup_url_is_exist(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code,200)

    def test_signup_use_signup_template(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response, 'signup.html')


class FunctionalTestIndex(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')
        #self.browser = webdriver.Chrome()

    def tearDown(self):
        self.browser.quit()

    def test_title_home(self):
        self.browser.get(self.live_server_url)
        time.sleep(3)
        self.assertIn('Home', self.browser.title)
        time.sleep(3)

    def test_all_functions(self):
        self.browser.get(self.live_server_url)
        time.sleep(3)
        self.browser.find_element_by_id("signup").click()
        time.sleep(3)
        self.assertIn('Sign Up', self.browser.title)
        self.browser.find_element_by_id("id_username").send_keys("akishi")
        self.browser.find_element_by_id("id_first_name").send_keys("Nadhif")
        self.browser.find_element_by_id("id_last_name").send_keys("Suyudi")
        self.browser.find_element_by_id("id_email").send_keys("nadhifsuyudi@gmail.com")
        self.browser.find_element_by_id("id_password1").send_keys("setsura15")
        self.browser.find_element_by_id("id_password2").send_keys("setsura15")
        self.browser.find_element_by_id("signup_button").click()
        time.sleep(3)
        self.assertIn('Login', self.browser.title)
        self.browser.find_element_by_id("id_username").send_keys("akishi")
        self.browser.find_element_by_id("id_password").send_keys("setsura15")
        self.browser.find_element_by_id("login_button").click()
        time.sleep(3)
        self.assertIn('Home', self.browser.title)
        self.browser.find_element_by_id("logout").click()
        time.sleep(3)
