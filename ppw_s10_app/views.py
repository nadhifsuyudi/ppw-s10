from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic

from .forms import SignUpForm, ProfileForm
# Create your views here.

def home(request):
   return render(request, 'home.html')

class SignUp(generic.CreateView):
    form_class = SignUpForm
    form_photo = ProfileForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'